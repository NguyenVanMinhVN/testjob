export interface IPosts{
    id?:string;
    tag?: string[];
    author?: string;
    tiltle?: string;
    subtiltle?: string;
    content?: string;
    image_url?: string;
    is_deleted?: boolean
}