import { Body, Controller, Delete, Get, Param, Post, Put, Query } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CategoryService } from './category.service';
import { Auth } from 'src/decorators/role.decorator';
import { CategoryDto } from './category.dto';

@ApiBearerAuth('Authorization')
@ApiTags('Category managements')
@Controller('category')
export class CategoryController {
    constructor(private readonly categoryService: CategoryService){}
    @Post()
    @Auth()
    async creatTag(@Body() body: CategoryDto){
        return await this.categoryService.creatCategory(body);
    }

    @Put(':id')
    @Auth()
    @ApiOperation({summary:"Update category"})
    async udatedTag(@Param('id') id: string,@Body() body: CategoryDto){
        return await this.categoryService.updatedCategory(id, body);
    }

    @Get('list')
    // @Auth()
    async findAllTag(@Query() query: CacheQueryOptions){
        console.log("findAllCategory");
        return this.categoryService.fillAllCategory(query);
    }

    @Delete(':id')
    @Auth()
    async deleteTag(@Param('id') id: string ){
        return this.categoryService.deleteCategory(id, {is_deleted: true});
    }
}
