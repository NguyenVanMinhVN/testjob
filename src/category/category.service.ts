import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Categorys } from './category.schema';
import { PaginateModel } from 'mongoose';
import { Icategory } from './category.interface';
import { filterRequest, optionsRequest } from 'src/helpers/units';

@Injectable()
export class CategoryService {
    constructor(
        @InjectModel(Categorys.name) private readonly categoryModel: PaginateModel<Categorys>,
      ) {}

      async creatCategory(body: Icategory) {
        return this.categoryModel.create(body);
      }

      async updatedCategory(id: string ,body: Icategory) {
        return this.categoryModel.findByIdAndUpdate(id, body);
      }

      async deleteCategory(id: string, body: Icategory){
        const post = await this.categoryModel.findOne({_id: id, is_deleted: false})
        if(!post){
            throw new NotFoundException("Not Found Post")
        }
        return await this.categoryModel.findByIdAndUpdate(id, body,{new: true})
      }

      async fillAllCategory(queryObj: any){
        const query = filterRequest(queryObj, true)
        let options = optionsRequest(queryObj)
        if(queryObj.limit&&queryObj.limit === '0'){
            options.pagination = false;
        }
        return await this.categoryModel.paginate(query,options).then(data=>({...data, docs: data.docs.map(item => item.view())}))
      }
}
