import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsOptional, IsString } from "class-validator";
import { CommonQueryDto } from "src/shared/share.dto";

export class CategoryDto{
    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    name: string;

    @ApiProperty()
    @IsOptional()
    @IsString()
    status: string;
}

export class CategoryQueryDto extends CommonQueryDto{

    @ApiProperty({ description: 'name', required: false})
    @IsOptional()
    @IsString()
    name: string;
}