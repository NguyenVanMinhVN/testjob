import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Icategory } from "./category.interface";
import * as mongoseePaginate from 'mongoose-paginate-v2'
import { Document } from "mongoose";
import { EStatus } from "src/shared/share.enum";

@Schema({timestamps:{createdAt: 'created_at', updatedAt: 'updated_at'}})
export class Categorys extends Document{
    @Prop({type: String})
    id: string;
    @Prop({type: String})
    name: string;
    @Prop({type: Boolean, default: false})
    is_deleted: boolean;
    @Prop({type: String, default: EStatus.ACTIVE})
    status: string;
    view: (full?: boolean) => Icategory
}

export const CategorySchema = SchemaFactory.createForClass(Categorys);
CategorySchema.methods.view = function (full?: boolean): Icategory{
    const view: Icategory = {
        id : this._id,
        name: this.name,
        status: this.status
    }
    return full ? {...view} :view;
}

CategorySchema.plugin(mongoseePaginate);