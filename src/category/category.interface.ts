export interface Icategory {
    id?: string;
    name?: string;
    status?: string;
    is_deleted?: boolean
}