import { Module } from '@nestjs/common';
import { CategoryController } from './category.controller';
import { CategoryService } from './category.service';
import { MongooseModule } from '@nestjs/mongoose';
import { CategorySchema, Categorys } from './category.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
        {
            name: Categorys.name,
            schema: CategorySchema
        }
    ])
],
  controllers: [CategoryController],
  providers: [CategoryService]
})
export class CategoryModule {}
