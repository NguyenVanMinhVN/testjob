import { URL, parse } from 'url';
import * as jwt from 'jsonwebtoken';
import { BadRequestException } from '@nestjs/common';



export const generateToken = async (data: any, secretSignature: string, tokenLife: string): Promise<string> => {
    return new Promise(async (resolve, reject) => {
        try {
            if (!data || !secretSignature || !tokenLife) {
                throw new BadRequestException('Missing required parameters');
            }

            const token = jwt.sign(
                data,
                secretSignature,
                {
                    algorithm: "HS256",
                    expiresIn: tokenLife,
                }
            );

            resolve(token);
        } catch (error) {
            reject(error);
        }
    });
};
export const verifyToken = async (bearToken: string, secretKey: string): Promise<any> => {
    try {
        if (!bearToken || !secretKey) {
            throw new BadRequestException('Missing required parameters');
        }

        const tokenPrefix = 'Bearer ';
        if (!bearToken.startsWith(tokenPrefix)) {
            throw new BadRequestException('Invalid token format');
        }
        const token = bearToken.slice(tokenPrefix.length);

        const decoded = await jwt.verify(token, secretKey);
        return decoded;
    } catch (error) {
        throw new BadRequestException(error.message);
    }
};
export function convertDateTime(valDate, typeDate) {
    if (valDate) {
        let date_val = new Date(valDate);
        if (typeDate === 0) {
            date_val.setHours(0, 0, 0, 0);
        } else {
            date_val.setHours(23, 59, 59, 999);
        }
        valDate = date_val.toISOString();
    }
    return valDate;
}








export function filterRequest(params: any, deleted: boolean) {
    let query: { [key: string]: any } = {};
    if (deleted) {
        query.is_deleted = false;
    }

    if (Object.keys(params).length) {
        for (let key in params) {
            if (key === 'token') delete params.token;
            if (typeof params[key] === 'object') {
                let objQuery = params[key];
                for (let objKey in objQuery) {
                    if (objQuery[objKey]) {
                        if (!query[key]) query[key] = {};
                        let addQuery = query[key];
                        if (objKey === 'from') {
                            let from = convertDateTime(objQuery.from, 0);
                            addQuery.$gte = new Date(from);
                        } else if (objKey === 'to') {
                            let to = convertDateTime(objQuery.to, 1);
                            addQuery.$lte = new Date(to);
                        } else if (objKey === 'min') {
                            addQuery.$gte = +objQuery[objKey];
                        } else if (objKey === 'max') {
                            addQuery.$lte = +objQuery[objKey];
                        } else if (objKey === 'min_ne') {
                            addQuery.$gt = +objQuery[objKey];
                        } else if (objKey === 'max_ne') {
                            addQuery.$lt = +objQuery[objKey];
                        }
                        if (objKey === 'from-time') {
                            let from = convertDateTime(objQuery['from-time'], 2);
                            addQuery.$gt = new Date(from);
                        } else if (objKey === 'to-time') {
                            let to = convertDateTime(objQuery['to-time'], 2);
                            addQuery.$lt = new Date(to);
                        } else if (objKey === 'equal') {
                            query[key] = objQuery[objKey];
                        } else if (objKey === 'in') {
                            let data = objQuery[objKey];
                            query[key] = {
                                $in: data.split(','),
                            };
                        } else if (objKey === 'nin') {
                            let data = objQuery[objKey];
                            query[key] = {
                                $nin: data.split(','),
                            };
                        } else if (objKey === 'like') {
                            query[key] = new RegExp(objQuery.like, 'i');
                        }
                    }
                }
            } else if (
                key !== 'sort_by' &&
                key !== 'order_by' &&
                key !== 'page' &&
                key !== 'limit'
            ) {
                query[key] = params[key];
            }
        }
    }
    return query;
}
export interface IOptionsRequest {
    page: number;
    limit: number;
    pagination?: boolean;
    populate?: string[];
}
export function optionsRequest(params): IOptionsRequest {
    const { page, limit } = params;

    let options = {
        page: parseInt(page, 10) || 1,
        limit: parseInt(limit, 10) || 10,
        sort: {},
    };
    options.sort = { created_at: -1 };

    if (params.sort_by) {
        options.sort = {
            [params.sort_by]: -1,
        };

        if (params.order_by) {
            options.sort = {
                [params.sort_by]: parseInt(params.order_by),
            };
        }
    }

    return options;
}