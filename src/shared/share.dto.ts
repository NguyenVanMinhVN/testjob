import { ApiProperty } from "@nestjs/swagger";
import { IsOptional, IsString } from "class-validator";
import { EStatus } from "./share.enum";
import { EuserStatus } from "src/users/user.enum";

export class CommonQueryDto {
    @IsString()
    @IsOptional()
    @ApiProperty({ description: 'Page number', required: false})
    page: number;

    @IsString()
    @IsOptional()
    @ApiProperty({ description: 'Page size', required: false})
    limit: number;

    @ApiProperty({ description: 'Status', required: false, type: String, enum: EStatus})
    @IsOptional()
    @IsString()
    status: EuserStatus;
}