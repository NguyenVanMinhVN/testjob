import { Body, Controller, Delete, Get, Param, Post, Put, Query } from "@nestjs/common";
import { UserService } from "./user.service";
import { query } from "express";
import { UpdateUserDto, UserQuery } from "./user.dto";
import { ApiBearerAuth, ApiTags } from "@nestjs/swagger";
import { Auth } from "src/decorators/role.decorator";
import { CacheTTL } from "@nestjs/cache-manager";

@Controller('admin/users')
@ApiTags('Admin Users')
@ApiBearerAuth('Authorization')
@Auth()
export class AdminUserController{
    constructor(private readonly userService: UserService){

    }
    @Get('list')
    async getListUser(@Query() query: UserQuery){
        return await this.userService.findAll(query);
    }
    @Put(':id')
    @CacheTTL(30)
    async findUser(@Param('id') id: string, @Body() body: UpdateUserDto){
        console.log('id : ',id)
        return await this.userService.updateUser(id, body);
    }
    @Delete(':id')
    async deleteUser(@Param('id') id: string){
        return await this.userService.updateUser(id,{is_deleted: true});
    }
}